<?php

use PeterNikonov\Invoice\Entity\Product\LogoService;
use PeterNikonov\Invoice\Entity\Reward;
use PeterNikonov\Invoice\Entity\Worker;
use PeterNikonov\Invoice\Invoice;
use PeterNikonov\Invoice\Item\LogoServiceItem;
use PeterNikonov\Invoice\Process\LogoRewardCalculator;
use PHPUnit\Framework\TestCase;

class LogoRewardCalculatorTest extends TestCase
{
    public function getRewardInstance() : Reward {
        $reward = new Reward();
        $reward->setRate(500);
        $reward->setMethod(Reward::METHOD_FIX);
        $reward->setLevel(Reward::LEVEL_ASSISTANCE);
        $reward->setDay(Reward::ANYDAY);

        return $reward;
    }

    public function getLogoServiceInstance($price = 0) : LogoService {

        $logoService = new LogoService();
        $logoService->setId(1);
        $logoService->setName('Услуга');
        $logoService->setPrice($price);
        $logoService->setCategory(1);
        $logoService->setSection(LogoService::CABINET_SECTION);
        $logoService->setOrganization(1);
        $logoService->setDivision(1);
        $logoService->setRequieredAssistant(true);
        $logoService->setContractPrice(800);

        $reward = new Reward();
        $reward->setRate(500);
        $reward->setMethod(Reward::METHOD_FIX);
        $reward->setLevel(Reward::LEVEL_ASSISTANCE);
        $reward->setDay(Reward::ANYDAY);

        $logoService->setReward($reward);

        $reward = new Reward();
        $reward->setRate(100);
        $reward->setMethod(Reward::METHOD_FIX);
        $reward->setLevel(Reward::LEVEL_PRIMARY);
        $reward->setDay(Reward::ANYDAY);

        $logoService->setReward($reward);

        return $logoService;
    }

    public function getWorkerInstance() : Worker {

        $reward = $this->getRewardInstance();

        $worker = new Worker();
        $worker->setId(1);
        $worker->setName('Вася');
        $worker->setReward($reward);

        return $worker;
    }

    public function getItemInstance($amount = 1, $price = 1000, $discount = 0) {

        $logoServiceItem = new LogoServiceItem;

        $logoServiceItem->setAmount($amount);
        $logoServiceItem->setDiscountPercent($discount);

        $logoServiceItem->setWorker($this->getWorkerInstance());
        $logoServiceItem->setAssistant($this->getWorkerInstance());
        $logoServiceItem->setProduct($this->getLogoServiceInstance($price));

        return $logoServiceItem;
    }

    /**
     * Подготовим тестируемый объект
     * @return Invoice
     */
    public function getInvoiceObject(array $items): Invoice
    {

        $invoice = new Invoice();
        foreach ($items as $item) {
            $invoice->setItem($item);
        }

        return $invoice;
    }

    public function testCountSum()
    {
        $items = [
            $this->getItemInstance(1, 1000, 0),
            $this->getItemInstance(1, 1000, 0),
        ];

        $calculator = new LogoRewardCalculator($this->getInvoiceObject($items));
        $calculate = $calculator->calculate();

        $rewardSum = [];
        foreach ($calculate as $reward) {
            $rewardSum[] = $reward['reward_sum'];
        }

        $totalInvoiceRewardSum = array_sum($rewardSum);

        $this->assertEquals(1200, $totalInvoiceRewardSum);

    }
}
