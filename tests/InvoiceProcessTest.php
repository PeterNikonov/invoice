<?php

use PeterNikonov\Invoice\Entity\Product\LogoService;
use PeterNikonov\Invoice\Entity\Reward;
use PeterNikonov\Invoice\Entity\Worker;
use PeterNikonov\Invoice\Invoice;
use PeterNikonov\Invoice\Item\LogoServiceItem;
use PeterNikonov\Invoice\Process\InvoiceProcess;
use PHPUnit\Framework\TestCase;

class InvoiceProcessTest extends TestCase
{
    public function getRewardInstance() : Reward {
        $reward = new Reward();

        return $reward;
    }

    public function getLogoServiceInstance($price = 0) : LogoService {

        $reward = $this->getRewardInstance();

        $logoService = new LogoService();
        $logoService->setId(1);
        $logoService->setName('Услуга');
        $logoService->setPrice($price);
        $logoService->setCategory(1);
        $logoService->setSection(LogoService::CABINET_SECTION);
        $logoService->setOrganization(1);
        $logoService->setDivision(1);
        $logoService->setRequieredAssistant(true);
        $logoService->setContractPrice(800);
        $logoService->setReward($reward);

        return $logoService;
    }

    public function getWorkerInstance() : Worker {

        $reward = $this->getRewardInstance();

        $worker = new Worker();
        $worker->setId(1);
        $worker->setName('Вася');
        $worker->setReward($reward);

        return $worker;
    }

    public function getItemInstance($amount = 1, $price = 1000, $discount = 0) {

        $logoServiceItem = new LogoServiceItem;

        $logoServiceItem->setAmount($amount);
        $logoServiceItem->setDiscountPercent($discount);

        $logoServiceItem->setWorker($this->getWorkerInstance());
        $logoServiceItem->setAssistant($this->getWorkerInstance());
        $logoServiceItem->setProduct($this->getLogoServiceInstance($price));

        return $logoServiceItem;
    }

    /**
     * Подготовим тестируемый объект
     * @return Invoice
     */
    public function getInvoiceObject(array $items): Invoice
    {

        $invoice = new Invoice();
        foreach ($items as $item) {
            $invoice->setItem($item);
        }

        return $invoice;
    }

    public function testCountSum()
    {
        $items = [
            $this->getItemInstance(1, 1000, 0),
            $this->getItemInstance(1, 1000, 0),
        ];

        $process = new InvoiceProcess($this->getInvoiceObject($items));

        $this->assertEquals('2000', $process->countSum());
    }
}
