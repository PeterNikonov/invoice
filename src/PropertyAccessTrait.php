<?php

namespace PeterNikonov\Invoice;

trait PropertyAccessTrait
{
    /**
     * Return value of concrete property if exist
     *
     * @param $property
     * @return mixed
     * @throws \Exception
     */
    public function __get($property)
    {
        if (!property_exists($this, $property)) {
            throw new \Exception('Property ' . $property . ' not exists');
        }

        return $this->$property;
    }
}
