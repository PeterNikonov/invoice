<?php

namespace PeterNikonov\Invoice\Entity;

class Assertion
{
    const USE_CONTRACT_PRICE = 'use contract price';
    const USE_DMS_PRICE = 'use dms price';
}