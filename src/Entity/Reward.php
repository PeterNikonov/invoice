<?php

namespace PeterNikonov\Invoice\Entity;
use PeterNikonov\Invoice\PropertyAccessTrait;

/**
 * Принципы расчета вознаграждения
 *
 * Class Reward
 * @package PeterNikonov\Invoice\Entity
 */
class Reward
{
    use PropertyAccessTrait;

    # расчет по проценту или фиксированная сумма
    const METHOD_PERCENT = 0;
    const METHOD_FIX = 1;
    # специалист или ассистент
    const LEVEL_PRIMARY = 0;
    const LEVEL_ASSISTANCE = 1;
    # признак день недели
    const ANYDAY = 0;
    const WEEKDAY = 1;
    const HOLIDAY = 2;

    /**
     * Ставка
     * @var int
     */
    protected $rate = 0;
    /**
     * Способ расчета
     * @var int
     */
    protected $method = self::METHOD_FIX;
    /**
     * Ранг сотрудника
     * @var int
     */
    protected $level = self::LEVEL_PRIMARY;
    /**
     * Тип дня недели для применения ставки
     * @var int
     */
    protected $day = self::ANYDAY;

    /**
     * @return int
     */
    public function getMethod(): int
    {
        return $this->method;
    }

    /**
     * @param int $method
     */
    public function setMethod(int $method): void
    {
        $this->method = $method;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate(int $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->day;
    }

    /**
     * @param int $day
     */
    public function setDay(int $day): void
    {
        $this->day = $day;
    }
}
