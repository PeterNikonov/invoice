<?php

namespace PeterNikonov\Invoice\Entity\Product;

use PeterNikonov\Invoice\Entity\Reward;

class LogoService extends Service
{
    const DEFAULT_SECTION = 0;
    const CABINET_SECTION = 1;
    const TOMATIS_SECTION = 2;
    /**
     * @var Reward[]
     */
    protected $rewards = [];
    /**
     * Требует ассистента
     * @var bool
     */
    protected $requieredAssistant = false;
    /**
     * Для какого типа записи доступна
     * @var array
     */
    protected $sections = [];
    /**
     * Какой организации относится
     * @var int
     */
    protected $organization = 0;
    /**
     * Какому подразделению относится
     * @var array
     */
    protected $divisions = [];
    /**
     * Цена при оплате через депозит
     * @var int
     */
    protected $contractPrice = 0;

    /**
     * @return Reward[]
     */
    public function getRewards(): array
    {
        return $this->rewards;
    }

    /**
     * @param Reward $reward
     */
    public function setReward(Reward $reward): void
    {
        $this->rewards[] = $reward;
    }

    /**
     * @return bool
     */
    public function isRequieredAssistant(): bool
    {
        return $this->requieredAssistant;
    }

    /**
     * @param bool $requieredAssistant
     */
    public function setRequieredAssistant(bool $requieredAssistant): void
    {
        $this->requieredAssistant = $requieredAssistant;
    }

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param int $sections
     */
    public function setSection(int $sections): void
    {
        $this->sections[] = $sections;
    }

    /**
     * @return int
     */
    public function getOrganization(): int
    {
        return $this->organization;
    }

    /**
     * @param int $organization
     */
    public function setOrganization(int $organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return array
     */
    public function getDivisions(): array
    {
        return $this->divisions;
    }

    /**
     * @param int $division
     */
    public function setDivision(int $division): void
    {
        $this->divisions[] = $division;
    }

    /**
     * @return int
     */
    public function getContractPrice(): int
    {
        return $this->contractPrice;
    }

    /**
     * @param int $contractPrice
     */
    public function setContractPrice(int $contractPrice): void
    {
        $this->contractPrice = $contractPrice;
    }
}
