<?php

namespace PeterNikonov\Invoice\Entity\Product;

interface ProductInterface
{
    public function getPrice() : int;
}
