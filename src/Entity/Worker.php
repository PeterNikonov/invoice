<?php

namespace PeterNikonov\Invoice\Entity;

class Worker
{
    protected $id = 0;
    protected $name = '';
    /**
     * @var Reward[]
     */
    protected $rewards = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Reward[]
     */
    public function getRewards(): array
    {
        return $this->rewards;
    }

    /**
     * @param Reward $reward
     */
    public function setReward(Reward $reward): void
    {
        $this->rewards[] = $reward;
    }
}
