<?php

namespace PeterNikonov\Invoice\Item;

use PeterNikonov\Invoice\Entity\Worker;
use PeterNikonov\Invoice\Entity\Product\Service;

/**
 * Услуга в счете
 *
 * Class CollectiveServiceItem
 * @package PeterNikonov\Invoice
 *
 * @property Service $product
 */
class ServiceItem extends Item
{
    /**
     * Doctor - primary.
     * @var Worker
     */
    protected $worker;

    /**
     * @return Worker
     */
    public function getWorker(): Worker
    {
        return $this->worker;
    }

    /**
     * @param Worker $worker
     */
    public function setWorker(Worker $worker): void
    {
        $this->worker = $worker;
    }
}
