<?php

namespace PeterNikonov\Invoice\Item;

use PeterNikonov\Invoice\Entity\Product\LogoService;
use PeterNikonov\Invoice\Entity\Product\ProductInterface;
use PeterNikonov\Invoice\Entity\Worker;

/**
 * Услуга в счете с ассистентом
 *
 * Class CollectiveServiceItem
 * @package PeterNikonov\Invoice
 *
 * @property LogoService $product
 */
class LogoServiceItem extends ServiceItem
{
    /**
     * Assist.
     * @var Worker
     */
    protected $assistant;

    /**
     * @return Worker
     */
    public function getAssistant(): Worker
    {
        return $this->assistant;
    }

    /**
     * @param Worker $assistant
     */
    public function setAssistant(Worker $assistant): void
    {
        $this->assistant = $assistant;
    }
}
