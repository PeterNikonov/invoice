<?php

namespace PeterNikonov\Invoice\Item;

use PeterNikonov\Invoice\Entity\Assertion;
use PeterNikonov\Invoice\Entity\Product\LogoService;
use PeterNikonov\Invoice\Entity\Product\Service;
use PeterNikonov\Invoice\Invoice;
use PeterNikonov\Invoice\Entity\Product\ProductInterface;

/**
 * Class Item
 * @package PeterNikonov\Invoice\Item
 */
class Item implements ProductInterface
{
    /**
     * @var integer
     */
    protected $amount = 1;
    /**
     * @var ProductInterface
     */
    protected $product;
    /**
     * @var integer
     */
    protected $discountPercent = 0;
    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * @return int
     */
    public function countSum(): int
    {
        $discount = $this->countDiscount();
        return ceil(($this->getPrice() * $this->amount) - $discount);
    }

    public function countDiscount(): int
    {
        if ($this->discountPercent == 0) {
            return 0;
        }

        return ceil(($this->getPrice() / 100) * $this->discountPercent) * $this->amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount) {
        $this->amount = $amount;
    }

    /**
     * @return ProductInterface|Service|LogoService
     */
    public function getProduct() : ProductInterface
    {
        return $this->product;
    }

    /**
     * @param ProductInterface $product
     */
    public function setProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getDiscountPercent(): int
    {
        return $this->discountPercent;
    }

    /**
     * @param int $discountPercent
     */
    public function setDiscountPercent(int $discountPercent): void
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    public function getPrice(): int
    {
        if ($this->invoice->hasClaim(Assertion::USE_CONTRACT_PRICE)) {
            if ($this->product instanceof LogoService) {
                return $this->product->getContractPrice();
            }
        }

        return $this->product->getPrice();
    }
}
