<?php

namespace PeterNikonov\Invoice\Process;

use PeterNikonov\Invoice\Entity\Reward;

class RewardsProcess
{
    /**
     * @var Reward[]
     */
    private $rewards;

    public function __construct(array $rewards)
    {
        $this->rewards = $rewards;
    }

    /**
     * Filter by property value.
     *
     * @param string $property
     * @param $value
     * @return $this
     */
    public function filterBy(string $property, $value)  {
        $this->rewards = array_filter($this->rewards, function(Reward $reward) use ($property, $value) {
            return $reward->$property == $value;
        });

        return $this;
    }

    /**
     * @return array|Reward[]
     */
    public function getRewards() {
        return $this->rewards;
    }
}
