<?php

namespace PeterNikonov\Invoice\Process;

use PeterNikonov\Invoice\Entity\Reward;
use PeterNikonov\Invoice\Invoice;
use PeterNikonov\Invoice\Item\LogoServiceItem;

class LogoRewardCalculator
{
    protected $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    protected function getItemReward(LogoServiceItem $item, $level, $method, $day)
    {
        $rewards = $item->getProduct()->getRewards();

        $rewardProcess = new RewardsProcess($rewards);
        $rewardProcess->filterBy('level', $level)
            ->filterBy('method', $method)
            ->filterBy('day', $day);

        $filtered = $rewardProcess->getRewards();
        $reward = array_pop($filtered);

        return [
            'worker_id' => $item->getAssistant()->getId(),
            'reward_sum' => $reward->getRate() * $item->getAmount(),
            'service_id' => $item->getProduct()->getId(),
        ];
    }

    /**
     * Условия вознаграждения определяет услуга
     */
    public function calculate()
    {
        /**
         * @var LogoServiceItem[] $items
         */
        $items = $this->invoice->getItems();
        foreach ($items as $item) {
            // если предусмотрен ассистент
            if ($item->getProduct()->isRequieredAssistant()) {
                $result[] = $this->getItemReward($item, Reward::LEVEL_ASSISTANCE, Reward::METHOD_FIX, Reward::ANYDAY);
            }
            // для основного сотрудника
            $result[] = $this->getItemReward($item, Reward::LEVEL_PRIMARY, Reward::METHOD_FIX, Reward::ANYDAY);
        }

        return $result;
    }
}
