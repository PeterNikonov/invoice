<?php

namespace PeterNikonov\Invoice\Process;

use PeterNikonov\Invoice\Invoice;

class InvoiceProcess
{
    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * InvoiceProcess constructor.
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Получить сумму по счету
     */
    public function countSum() : int
    {
        $arr = [];

        $items = $this->invoice->getItems();
        foreach ($items as $item) {
            $arr[] = $item->countSum();
        }

        return array_sum($arr);
    }

    /**
     * Расчитать вознаграждения по счет, на основании данных для расчета установленных для счета
     *
     * @return array
     */
    public function calculateRewardByItem() : array
    {
    }

    /**
     * Расчитать вознаграждения по счет, на основании данных для расчета установленных для специалиста
     *
     * @return array
     */
    public function calculateRewardByService() : array
    {
    }
}
