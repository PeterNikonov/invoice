<?php

namespace PeterNikonov\Invoice;

use function in_array;

trait ClaimTrait
{
    /**
     * @var array
     */
    protected $claims = [];

    /**
     * @param string $key
     */
    public function hasClaim($value = '') : bool {
        return in_array($value, $this->claims);
    }

    public function add($value) {
        if(!$this->hasClaim($value)) {
            $this->claims[] = $value;
        }
    }

    public function remove($value) {
        if($this->hasClaim($value)) {
            foreach ($this->claims as $key => $assertion) {
                if($value == $assertion) {
                    unset($this->claims[$key]);
                }
            }
        }
    }
}
