<?php

namespace PeterNikonov\Invoice;

use DateTime;
use PeterNikonov\Invoice\Item\Item;

class Invoice
{
    use ClaimTrait;
    /**
     * @var integer
     */
    protected $id = 0;
    /**
     * @var int
     */
    protected $division = 0;
    /**
     * @var integer
     */
    protected $client = 0;
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var Item[]
     */
    protected $items = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDivision(): int
    {
        return $this->division;
    }

    /**
     * @param int $division
     */
    public function setDivision(int $division): void
    {
        $this->division = $division;
    }

    /**
     * @return int
     */
    public function getClient(): int
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient(int $client): void
    {
        $this->client = $client;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item[] $item
     */
    public function setItem(Item $item): void
    {
        $item->setInvoice($this);
        $this->items[] = $item;
    }
}
